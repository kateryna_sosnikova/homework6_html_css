
document.addEventListener('DOMContentLoaded', function () {

    let changeTheme = document.createElement('button');
    let logo = document.querySelector('.logo-left-container');
    changeTheme.innerText = 'Сменить тему';
    changeTheme.setAttribute("style", "color : white; background-color: green; margin-left: 20px; padding: 5px;");
    logo.append(changeTheme);

    let styleTheme = (theme) => {
        document.querySelector('.navigation-menu').style.backgroundColor = getTheme(theme).nav;
        document.querySelector('.button-subscribe').style.backgroundColor = getTheme(theme).btn;
        document.querySelector('.button-article').style.backgroundColor = getTheme(theme).btn2;
    }

    if (localStorage.getItem('theme') !== null) {
        let savedTheme = localStorage.getItem('theme');
        styleTheme(savedTheme);
    }

    changeTheme.onclick = function () {
        let local = localStorage.getItem('theme');

        let currentTheme = (local && local === 'light') ? 'dark' : 'light';

        localStorage.setItem('theme', currentTheme);

        styleTheme(currentTheme);
    }

    function getTheme(name) {

        let theme = {
            dark: {
                nav: '#313131',
                btn: '#89CA62',
                btn2: '#14B9D5',
            },
            light: {
                nav: '#de4707',
                btn: '#d6ac13',
                btn2: '#122e06',
            },
        }

        return theme[name];
    }

});


$(document).ready(function () {
    const headerMenuHeight = $('.header-menu').outerHeight();
    const scrollButton = $('.scroll-to-top');
    const hideButton = $('.hide-section');
   
    $('.header-menu li a').click(function (event) {
        event.preventDefault();
        $('html, body').animate({
            scrollTop: $(this.hash).offset().top - headerMenuHeight
        }, 1000)
    });

    $(window).scroll(function (event) {
        if ($('html').scrollTop() > $(window).height()) {
            scrollButton.addClass('shown');
        } else {
            scrollButton.removeClass('shown');
        }
    })

    scrollButton.click(function (event) {
        $('html, body').animate({
            scrollTop: 0
        })
    });

    hideButton.click(function (event) {
        console.log($(this))
        $(`#${$(this).data("section")}`).slideToggle("slow")
    })
});

